# OpenWRT WRT1200AC Build 

## Current Build Status
[![pipeline status](https://gitlab.com/hkuchampudi/OpenWRT-WRT1200AC/badges/v18.06.2/pipeline.svg)](https://gitlab.com/hkuchampudi/OpenWRT-WRT1200AC/commits/v18.06.2)
[![coverage report](https://gitlab.com/hkuchampudi/OpenWRT-WRT1200AC/badges/v18.06.2/coverage.svg)](https://gitlab.com/hkuchampudi/OpenWRT-WRT1200AC/commits/v18.06.2)

## Table of Contents
- [OpenWRT WRT1200AC Build](#openwrt-wrt1200ac-build)
  * [Current Build Status](#current-build-status)
  * [Introduction](#introduction)
  * [Flashing Firmware Instructions](#flashing-firmware-instructions)
    + [Flashing from OEM Firmware](#flashing-from-oem-firmware)
    + [Flashing from an Existing OpenWRT Installation](#flashing-from-an-existing-openwrt-installation)
  * [Connecting to the Wi-Fi Network](#connecting-to-the-wi-fi-network)
  * [Logging Into Luci WebUI](#logging-into-luci-webui)
  * [Additional Suggestions](#additional-suggestions)
  * [Additional Resources](#additional-resources)

## Introduction
This repository is dedicated to building the OpenWRT firmware for the [WRT-1200AC](https://openwrt.org/toh/linksys/linksys_wrt1200ac) by Linksys. This firmware image pre-installs tools/applications that users will find useful like Ad-Blocking, Dynamic DNS support, DNS-over-TLS, SQM, Wireguard support, etc.

## Flashing Firmware Instructions
You can view the official firmware flashing instructions [here](https://openwrt.org/toh/linksys/wrt_ac_series).

### Flashing from OEM Firmware
1. Login to the Linksys WebUI (the default IP should be http://192.168.1.1/ with the password being `admin`).
2. Click on the **Connectivity** tab, and then click on **Manual Update**.
3. In another tab, visit the [Releases page](https://github.com/hkuchampudi/OpenWRT-WRT1200AC/releases) for this repository and download the `*-squashfs-factory.img` file for the latest release.
4. Back in the Linksys WebUI, **Select factory image** and select the file you downloaded in the previous step.
5. Wait for the router to flash the firmware. **DO NOT INTERRUPT THIS PROCESS!** Doing so may brick your device.
6. Once the firmware has finished flashing, the router will automatically reboot.
7. Once the router has fully booted, you should then be able to [connect to the Wi-Fi network](#Connecting-to-the-Wi-Fi-Network) and visit the Luci WebUI at https://192.168.1.1/.

### Flashing from an Existing OpenWRT Installation
1. Login to the Luci WebUI (Default IP: 192.168.1.1)
2. In another tab, visit the [Releases page](https://github.com/hkuchampudi/OpenWRT-WRT1200AC/releases) for this repository and download the `*-squashfs-sysupgrade.bin` file for the latest release.
3. Back in the Luci WebUI tab, click on the **System** tab, click on **Backup/Flash Firmware**. 
4. Then on the new page, select **Flash New Firmware Image** and then **Choose File**. Select the file downloaded in step 2.
5. Unselect **Keep Settings**
6. Select **Flash Image**
7. Wait for the router to flash the firmware. **DO NOT INTERRUPT THIS PROCESS!** Doing so may brick your device.
8. Once the firmware has finished flashing, the router will automatically reboot.
9. Once the router has fully booted, you should then be able to [connect to the Wi-Fi network](#Connecting-to-the-Wi-Fi-Network) and visit the Luci WebUI at https://192.168.1.1/.

## Connecting to the Wi-Fi Network
This firmware image creates two default WPA2 networks:
  - `OpenWRT` - for the 2.4Ghz network
  - `OpenWRT_5G` - for the 5Ghz network

You can connect to either of these networks using the default password: `openwrt`. **You should change these passwords as soon as possible**.

## Logging Into Luci WebUI
By default, you will be able to login to Luci using the username **root** with no password. Luci will warn you to set a password - **You should do this as soon as possible**.

## Additional Suggestions
- **Harden SSH Connection**: By default for diagnosing errors, the router is accessible via SSH on port 22 with the **root** username and set password (if no root password is set, no password is required). Users should use SSH keys instead of password-based SSH authentication for better security.
  - [OpenWRT Documentation](https://openwrt.org/docs/guide-user/base-system/ssh_configuration)
  - [Medium Article](https://medium.com/openwrt-iot/openwrt-how-to-set-up-dropbear-public-key-authentication-d206bbbc8ca7)

## Additional Resources
- [OpenWRT User Guide](https://openwrt.org/docs/guide-user/start)
